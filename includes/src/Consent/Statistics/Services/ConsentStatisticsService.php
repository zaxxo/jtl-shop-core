<?php

declare(strict_types=1);

namespace JTL\Consent\Statistics\Services;

use JTL\Abstracts\AbstractService;
use JTL\Consent\Statistics\Repositories\ConsentStatisticsRepository;

/**
 * Class ConsentStatisticsService
 *
 * @package JTL\Consent\Statistics\Services
 * @since 5.4.0
 */
class ConsentStatisticsService extends AbstractService
{
    /**
     * @param ConsentStatisticsRepository $consentStatisticsRepository
     */
    public function __construct(
        protected ConsentStatisticsRepository $consentStatisticsRepository = new ConsentStatisticsRepository()
    ) {
    }

    protected function getRepository(): ConsentStatisticsRepository
    {
        return $this->consentStatisticsRepository;
    }

    /**
     * @param int         $visitorID
     * @param array       $consents
     * @param string|null $date
     * @return void
     */
    public function saveConsentValues(int $visitorID, array $consents = [], ?string $date = null): void
    {
        $this->getRepository()->saveConsentValues(
            visitorID: $visitorID,
            eventDate: $date ?? \date('Y-m-d'),
            consents: $consents
        );
    }

    /**
     * @param string      $eventDateFrom
     * @param string|null $eventDateTo
     * @param array       $eventNames
     * @return object
     */
    public function getConsentStats(
        string $eventDateFrom,
        ?string $eventDateTo = null,
        array $eventNames = []
    ): object {
        $result        = new \stdClass();
        $chart         = [];
        $table         = [];
        $consentValues = $this->getRepository()->getConsentValues(
            [$eventDateFrom, ($eventDateTo ?? \date('Y-m-d'))],
            $eventNames
        );

        foreach ($consentValues as $consentDate => $consentDetails) {
            $details = '';
            foreach ($consentDetails->consents as $consentName => $consentValue) {
                $details .= $consentName . ': ' . $consentValue . ', ';
            }
            if (\strlen($details) > 1) {
                $details = \substr($details, 0, -2);
            }
            $obj             = new \stdClass();
            $obj->date       = $consentDate;
            $obj->acceptance = ($consentDetails->acceptedAll / $consentDetails->visitors) * 100;
            $chart[]         = $obj;

            $obj             = new \stdClass();
            $obj->date       = $consentDate;
            $obj->visitors   = $consentDetails->visitors;
            $obj->acceptance = $consentDetails->acceptedAll;
            $obj->consents   = $details;
            $table[]         = $obj;
        }
        $result->dataTable = $table;
        $result->dataChart = $chart;

        return $result;
    }

    /**
     * @param array $consents
     * @return bool
     */
    public function hasAcceptedAll(array $consents): bool
    {
        return !\in_array(false, $consents, true);
    }
}
