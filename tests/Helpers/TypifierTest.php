<?php

namespace Tests\Helpers;

use JTL\Helpers\Typifier;
use PHPUnit\Framework\TestCase;

class TypifierTest extends TestCase
{
    /**
     * @return void
     */
    public function testObjectify()
    {
        $resp = new \stdClass();
        $this->assertEquals($resp, Typifier::Objectify(null, $resp));
    }

    /**
     * @return void
     */
    public function testIntifyOrNull()
    {
        $this->assertEquals(null, Typifier::intifyOrNull(null));
        $this->assertEquals(2, Typifier::intifyOrNull('2.123'));
    }

    /**
     * @return void
     */
    public function testArrify()
    {
        $this->assertEquals([1], Typifier::arrify(1));
    }

    /**
     * @return void
     */
    public function testIntify()
    {
        $this->assertEquals(1, Typifier::intify(null, 1));
        $this->assertEquals(2, Typifier::intify('2.123'), 3);
        $this->assertEquals(3, Typifier::intify(null, 3));
    }

    /**
     * @return void
     */
    public function testTypeify()
    {
        $this->assertEquals(2, Typifier::typeify('2.34', 'integer'));
    }

    /**
     * @return void
     */
    public function testStringify()
    {
        $this->assertEquals('123', Typifier::stringify(123));
    }

    /**
     * @return void
     */
    public function testFloatify()
    {
        $this->assertEquals(0.00, Typifier::floatify(null));
    }

    /**
     * @return void
     */
    public function testBoolify()
    {
        $this->assertEquals(true, Typifier::boolify(1));
        $this->assertEquals(true, Typifier::boolify('Y'));
        $this->assertEquals(true, Typifier::boolify('yes'));
        $this->assertEquals(true, Typifier::boolify('ja'));
        $this->assertEquals(false, Typifier::boolify('n'));
        $this->assertEquals(false, Typifier::boolify('_blah'));
        $this->assertEquals(null, Typifier::boolify('_blah', null));
        $this->assertEquals(true, Typifier::boolify('_blah', 'Lötzinn'));
        $this->assertEquals(true, Typifier::boolify(null, 'Lötzinn'));
        $this->assertEquals(true, Typifier::boolify(null, true));
    }
}
