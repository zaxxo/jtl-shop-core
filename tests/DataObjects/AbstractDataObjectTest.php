<?php

namespace Tests\DataObjects;

use JTL\Catalog\Product\Artikel;
use PHPUnit\Framework\TestCase;

use function PHPUnit\Framework\assertIsArray;
use function PHPUnit\Framework\assertIsObject;
use function PHPUnit\Framework\assertSame;

class AbstractDataObjectTest extends TestCase
{
    public function provideDataArray(): array
    {
        return [
            'data' => [
                [
                    'id'   => 15,
                    'name' => 'Franzi',
                    'age'  => 24,
                    'cat'  => 1
                ]
            ]
        ];
    }

    /**
     * @dataProvider provideDataArray
     */
    public function testHydrate($data)
    {
        $product = $this->getMockBuilder(className: Artikel::class)
            ->disableOriginalConstructor()
            ->onlyMethods(methods: ['fuelleArtikel'])
            ->getMock();

        $dto = new DataObject();
        $dto->hydrate($data);
        $dto->setProduct($product);
        $this->assertSame('Franzi', $dto->getName());
        $this->assertSame(24, $dto->getAge());
        $this->assertTrue($dto->hasCat());
        $dto->setCat('n');
        $this->assertFalse($dto->hasCat());
        $dto->setCat('ja');
        $this->assertTrue($dto->hasCat());
        $this->assertInstanceOf(Artikel::class, $dto->getProduct());
    }

    /**
     * @dataProvider provideDataArray
     */
    public function testToObject($data)
    {
        $dto = new DataObject();
        $dto->hydrate($data);
        assertIsObject($dto->toObject());
    }

    public function testGetMapping()
    {
        $dto = new DataObject();
        assertIsArray($dto->getMapping());
    }

    /**
     * @dataProvider provideDataArray
     */
    public function testToArray($data)
    {
        $dto = new DataObject();
        $dto->hydrate($data);
        assertIsArray($dto->toArray());
        assertSame('Franzi', $dto->toArray()['cVorname']);
        assertIsArray($dto->toArray(false));
        assertSame('Franzi', $dto->toArray(false)['name']);
    }
}
