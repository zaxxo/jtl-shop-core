# Eigenes Template

    <style>
        .std-ref {
            font-size: 75%;
            font-family: "Consolas","Monaco",monospace;
            font-weight: 400;
            background-clip: border-box;
            background-color: rgb(255,255,255);
            border-left: 1px solid rgb(225,228,229);
            border-right: 1px solid rgb(225,228,229);
            border-top: 1px solid rgb(225,228,229);
            border-bottom: 1px solid rgb(225,228,229);
            border-collapse: collapse;
            border-spacing: 0px, 0px;
            box-sizing: border-box;
            padding: 0px 5px 0px;
            color: rgb(231, 76, 69) !important;
            white-space: nowrap;
            empty-cells: show;
        }
    </style>

## Was ist ein Template?

Das *Template* des Shops ist die "optische Grundstruktur" bzw. "das grundlegende Aussehen" Ihres Shops. Mit Hilfe des
*Templates* wird die Anordnung sämtlicher Elemente aller Shop-Seiten vorgegeben.

!!! note
    Das *Template* ist nicht zu verwechseln, mit dem *Theme*! Das *Theme* ist ein Teil des *Templates*, welches eine
    noch feinere Anpassung der Shop-Seiten ermöglicht.

## Der bevorzugte Weg zum eigenen Template: Child-Templates

Mit einem Child-Template können Sie **Style-, JavaScript- oder PHP**-Dateien des in JTL-Shop mitgelieferten
Standard-Templates "*NOVA*" **erweitern oder überschreiben**. Dabei werden alle Dateien des Templates von JTL-Shop
geladen, außer denjenigen, die Sie in Ihr Child-Template kopiert und somit "überschrieben" haben. In Ihrem
Child-Template können Sie einzelne Bereiche des Shops (`Smarty Blöcke`) überschreiben oder ergänzen. Bei einem Update
Ihres Shops werden die Dateien des Child-Templates nicht überschrieben, sodass Sie Ihre Änderungen nicht erneut
vornehmen müssen. Allerdings sollten Sie nach einem Update prüfen, ob Ihre Änderungen noch korrekt funktionieren.

Die Grundstruktur eines Child-Templates mit eigenem Theme sieht wie folgt aus:

    templates/NOVAChild/
    ├── js
    │   └── custom.js
    ├── themes
    │   ├── base
    │   │   ├── images
    │   ├── my-nova
    │   │   ├── sass
    │   │   └── _variables.scss
    │   │   └── my-nova.scss
    │   │   └── my-nova_crit.scss
    │   └── custom.css
    │   └── my-nova.css
    │   └── my-nova_crit.css
    ├── Bootstrap.php
    ├── preview.png
    ├── README.md
    └── template.xml

### Vorgehen am Beispiel von NOVAChild

Um ein eigenes Template zu entwickeln, empfiehlt JTL Ihnen sich die Vorlage von dem JTL Build-Server
[NOVAChild](https://build.jtl-shop.de/#template) herunterzuladen. Sie entpacken die komprimierte Datei in ihrem
Templates-Ordner und können direkt anfangen, das Aussehen ihres Shops zu modifizieren.

!!! hint
    Achten Sie darauf, dass der Ordner für das Child-Template **NOVAChild** heißt. Wenn Sie einen anderen Namen
    verwenden wollen, müssten Sie nach Umbenennen des Ordners auch den Namespace in der Bootstrap.php im Child-Template
    Ordner anpassen.

!!! caution
    Der Name darf keine Sonderzeichen enthalten, denn ab JTL Shop 5 müssen alle Klassennamen der
    [PSR-4 Spezifikation](https://www.php-fig.org/psr/psr-4/) folgen.

Im Unterordner `<Shop-Root>/templates/NOVAChild/` finden Sie die Datei `template.xml`.

Wenn Sie also beispielsweise das NOVA-Template von JTL-Shop erweitern möchten, sollte die `template.xml` wie folgt
aussehen:

``` hl_lines="8"
<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Template isFullResponsive="true">
    <Name>NOVAChild</Name>
    <Author>Max Mustermann</Author>
    <URL>https://www.mein-shop.de</URL>
    <Version>1.0.0</Version>
    <MinShopVersion>5.0.0</MinShopVersion>
    <Parent>NOVA</Parent>
    <Preview>preview.png</Preview>
    <Description>Dieses Template dient als Vorlage für ein eigenes Child-Template des NOVA.</Description>
</Template>
```

Dabei ist zu beachten, dass unter `<Parent>` das Eltern-Template (repräsentiert durch den Ordnernamen) eingetragen
wird, welches angepasst werden soll.

!!! hint
    Das Attribute **isFullResponsive="true|false"** im Tag `<Template>` kennzeichnet, dass sich Ihr neues Template
    vollständig responsive verhält, also automatisch an jede Auflösung anpasst.

    Wenn Sie Ihr Child-Template vom NOVA-Template ableiten, dann sollten Sie dies immer auf **true** einstellen. Das
    Attribut bewirkt bei der Einstellung auf **true**, dass im Backend die Option "*Standard-Template für mobile
    Endgeräte?*" nicht mehr ausgewählt werden kann und eine Warnung ausgegeben wird, falls dies (noch) so sein sollte.

Ab JTL-Shop Version 5.0.0 ist es bei allen Templates Konvention, dass der in der `template.xml` definierte Namen auch
dem Ordnernamen entsprechen muss. Darüber hinaus werden alle PHP-Dateien im Hauptverzeichnis des Templates über einen
Autoloader geladen. Auch der  Namespace in allen dort genutzten PHP-Dateien muss dabei immer dem Schema
`Template\<template-name>` entsprechen.

Wenn Sie Ihr Childtemplate z.B. in `MeinShopTemplate` umbenannt haben, müssen folgende Änderungen vorgenommen
werden:

1. `<Name>`-Attribut in der template.xml: `MeinShopTemplate`
2. Ordner des Templates: `<Shop-Root>/templates/MeinShopTemplate`
3. *namespace* in der Datei Bootstrap.php: `Template\MeinShopTemplate`

## Einstellungen im Childtemplate verbergen

Um Einstellungen aus dem Eltern-Template im Backend des Child-Templates zu verbergen, legen Sie die Einstellungen in
der Template.xml an und fügen die Attribute `override="true"` und `Editable="false"` hinzu. Im folgenden Beispiel
wollen wir die Einstellung "Animation für Schaltflächen beim Hovern" aus dem Nova für das Childtemplate ausblenden:

    <Settings>
        <Section Name="Theme" Key="theme">
            <Setting Description="Animation für Schaltflächen beim Hovern" Key="button_animated" Type="select" Value="N" override="true" Editable="false">
                <Option Value="Y">Ja</Option>
                <Option Value="N">Nein</Option>
            </Setting>
        </Section>
    </Settings>

## Ihr Template aktivieren

Wenn Sie nun alle Änderungen an Ihrem Child-Template vorgenommen haben, gehen Sie in das Backend von JTL-Shop.
Navigieren Sie im Backend des Shops zum Menü **Einstellungen -> Templates** und klicken Sie dort auf den Button
`Aktivieren` neben Ihrem Child-Template.

In der folgenden Eingabemaske können Sie nun im Abschnitt **Theme** Ihr Theme aus der Select-Box auswählen.
Auch andere Template-Einstellungen können Sie hier vornehmen. Klicken Sie anschließend am Ende der Seite auf
`Speichern`, um Ihr Template in Betrieb zu nehmen.

Nach dem Ändern von Template-Einstellungen und/oder dem Wechsel von Themes empfiehlt es sich, die entsprechenden
Zwischenspeicher des Shops zu leeren. Hierzu navigieren Sie im Backend-Menü auf den Menüpunkt "**System**" und klicken
auf "**Cache**". Wählen Sie hier "*Template*" in der dazugehörigen Checkbox aus. Anschließend klicken Sie am Ende der
Seite auf den Button `absenden`, um den Cache zu leeren.

Nun sollten Ihr Child-Template aktiviert sein, sodass Sie Ihre Änderungen in Ihrem JTL-Shop sehen können.

## Überschreiben bestehender Skripte

Falls Sie im Parent-Template definierte JavaScript-Dateien überschreiben möchten, fügen Sie dem File-Eintrag das
Attribut `override="true"` hinzu und erstellen Sie Ihre eigene Version der JavaScript-Datei im Unterverzeichnis `js/`.

``` hl_lines="13"
<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Template isFullResponsive="true">
    <Name>NOVAChild</Name>
    <Author>Max Mustermann</Author>
    <URL>https://www.mein-shop.de</URL>
    <Parent>NOVA</Parent>
    <Preview>preview.png</Preview>
    <Description>Dieses Template dient als Vorlage für ein eigenes Child-Template des NOVA.</Description>

    <Minify>
        <JS Name="jtl3.js">
            <File Path="js/global.js" override="true"/>
        </JS>
    </Minify>
</Template>
```

Dieses Beispiel würde bewirken, dass die Datei `js/global.js` Ihres Child-Templates anstelle der originalen Datei des
NOVA-Templates eingebunden wird. Ohne das **override**-Attribut würde die genannte Datei **zusätzlich** zur `global.js`
des Parent-Templates eingebunden werden.

Der Name des minifizierten und zum Browser übermittelten Javascripts (`jtl3.js`) ist eine feste Konstante und darf
nicht angepasst werden.

## Eigene Skripte nachladen

Um den Ladevorgang der Shop-Seiten nicht zu stark zu verzögern, sollten Sie zusätzliche Skripte Ihres Child-Templates
in der Templatedatei `footer.tpl` laden. Hierfür ist der Block `{block name='layout-footer-js'}` vorgesehen.

Wichtig ist, dass Sie hierbei Ihre Skripte *asynchron* laden. Fügen Sie dazu das Attribut `async` zu Ihren
`<script>`-Tags hinzu. (zum Beispiel: `<script src="my-script.js" async></script>`)

<a id="label_eigenestemplate_tpldateien"></a>

## Änderungen an Template-Dateien

Template-Dateien (Dateiendung `.tpl`) sollten nicht direkt geändert werden, da Ihr Shop sonst nicht mehr updatefähig
wäre. Stattdessen können Sie gezielt einzelne Abschnitte einer .tpl anpassen, indem Sie die entsprechenden
"*Smarty Blöcke*" überschreiben oder ergänzen.

### Anpassungen über Smarty Blöcke

"*Smarty Blöcke*" sind im Template-Code namentlich definierte Stellen, welche im Child-Template *erweitert* oder
*ersetzt* werden können. Möglich ist dies dank der Fähigkeit des Smarty-Frameworks, "*Vererbung*" von Templates zu
erlauben. Mehr dazu finden Sie auf der Seite
[Template Inheritance](https://www.smarty.net/docs/en/advanced.features.template.inheritance.tpl).

Beispielsweise können Sie im Header Ihres Shops individuelle Dateien laden, das Logo austauschen, oder das Menü
anpassen. Das NOVA-Template besitzt bereits viele vordefinierte *Smarty Blöcke*, welche Sie beliebig verändern können.

Smarty Blöcke sind in den Template-Dateien an folgender Struktur zu erkennen:

    {block name="<name des blocks>"}...{/block}

Wenn Sie nun eine bestimmte Template-Datei verändern möchten, kopieren Sie diese aus dem Parent-Template und fügen
Sie an der gleichen Stelle in Ihr Child-Template-Verzeichnis ein.

!!! attention
    Die Ordnerstruktur im Child-Template muss der des Parent-Templates entsprechen.

    Beispiel: `templates/NOVA/layout/header.tpl` -> `templates/NOVAChild/layout/header.tpl`

Möchten Sie beispielsweise den Seitenkopf Ihres Shops anpassen, erstellen Sie in Ihrem Child-Template-Verzeichnis
den Ordner `layout/` und darin die Datei `header.tpl`. Möchten Sie nun beispielsweise den Seitentitel verändern, finden
Sie in der `header.tpl` Ihres Child-Templates den Block '*layout-header-head-title*':

    <title itemprop="name">{block name='layout-header-head-title'}{$meta_title}{/block}</title>

Dieser Block kann nun auf drei verschiedene Arten geändert werden.

**1. "Ersetzen"**

    {block name="layout-header-head-title"}Mein Shop!{/block}

Hierbei wird der komplette Block ersetzt:

* **Ursprüngliche Ausgabe:** {$meta_title}
* **Neue Ausgabe:** Mein Shop!

**2. "Text hängen":**

    {block name="layout-header-head-title" append} Mein Shop!{/block}

Hier wird der eingegebene Text an den meta-title der Seite angehängt:

* **Ursprüngliche Ausgabe:** {$meta_title}
* **Neue Ausgabe:** {$meta_title} Mein Shop!

**3. "Text voranstellen":**

    {block name="layout-header-head-title" prepend}Mein Shop! {/block}

Hiermit wird der eingegebene Text dem meta-title der Seite vorangestellt:

* **Ursprüngliche Ausgabe:** {$meta_title}
***Neue Ausgabe:** Mein Shop! {$meta_title}

In Ihrem Child-Template befinden sich nur die Template-Dateien, die Sie verändert haben. Die komplette
Template-Struktur aus dem jeweiligen Parent-Template ist nicht erforderlich. Wird das Parent-Template aktualisiert,
beispielsweise durch ein offizielles Update, müssen nur wenige bis gar keine Anpassungen an Ihrem Child-Template
vorgenommen werden.

Weitere Infos zu Blocks finden Sie auf [smarty.net](https://www.smarty.net/docs/en/language.function.block.tpl).

### Eine ganze .tpl Datei ersetzen

!!! caution
    Änderungen ganzer Template-Dateien können weitreichende Folgen haben! Gehen Sie bei der Bearbeitung bitte sehr
    vorsichtig vor.

Wenn Sie eine Template-Datei ersetzen wollen, können Sie eine Datei mit gleichem Namen wie im Parent-Template erstellen,
aber den Inhalt selbst festlegen. Dieses Vorgehen kann sinnvoll sein, bringt aber auch Nachteile mit sich. Nach einem
Update Ihres Shops müssen Sie sicherstellen, dass die modifizierte Datei noch korrekt funktioniert. Gegebenenfalls
müssen Sie ihre Änderungen erneut anpassen.

### Eigenen CSS-Code einfügen

Wie man eigenen CSS-Code in das Child-Template einfügt, finden Sie hier: [Eigenes Theme](./eigenes_theme.md)