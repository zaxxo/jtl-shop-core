# HOOK_TOOLSGLOBAL_INC_SETZESPRACHEUNDWAEHRUNG_WAEHRUNG (109)

## Triggerpunkt

Am Ende der Linkerzeugung für Sprache- und Währung

## Parameter

* `JTL\Filter\ProductFilter` **oNaviFilter** - Artikelfilter
* `mixed` **oZusatzFilter** - zusätzliche Filter
* `array` **cSprachURL** - default `= []`
* `JTL\Catalog\Product\Artikel` **oAktuellerArtikel** - aktueller Artikel
* `int` **kSeite** - Seiten-ID
* `int` **kLink** - Link-ID
* `string` **AktuelleSeite** - aktuelle Seite