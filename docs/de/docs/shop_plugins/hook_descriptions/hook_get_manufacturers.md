# HOOK_GET_MANUFACTURERS (226)

## Triggerpunkt

Nach dem Holen der Liste aller Hersteller

## Parameter

* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"
* `array` **&cacheTags** - Umfang des Zwischenspeicherns
* `array` **&manufacturers** - Liste der Hersteller