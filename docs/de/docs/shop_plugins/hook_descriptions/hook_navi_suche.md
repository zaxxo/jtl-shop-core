# HOOK_NAVI_SUCHE (161)

## Triggerpunkt

Bevor die Anzahl der Artikel ermittelt wird

## Parameter

* `bool` **bExtendedJTLSearch** - Flag, welches die erweiterte Suche anzeigt
* `mixed` **&oExtendedJTLSearchResponse** - default `= null`
* `array` **&cValue** - Suchbegriff
* `int` **&nArtikelProSeite** - Anzahl der anzuzeigenden Ergebisse pro Seite
* `int` **nSeite** - aktuelle Seite
* `string` **nSortierung** - vom Benutzer gewählte Sortierung der Ergebnisse
* `int` **bLagerbeachten** - Flag, welches signalisiert, ob Lagerbestände beachtet werden sollen