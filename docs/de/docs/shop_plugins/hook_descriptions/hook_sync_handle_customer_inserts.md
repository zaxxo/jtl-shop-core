# HOOK_SYNC_HANDLE_CUSTOMER_INSERTS (349)

## Triggerpunkt

In ``JTL\dbeS\Sync\Customer::handleInserts`` nach Mapping des Kunden, bevor der Kunde in die Datenbank eingefügt wird.

## Parameter

* `JTL\Customer\Customer` **customer** - Das Kundenobjekt
* `array` **&attributes** - Die Kundenattribute