# HOOK_CALCULATESHIPPINGFEES (307)

## Triggerpunkt

Nach der Versandkostenberechnung durch die Standardmodule, aber - im Gegensatz zu
[HOOK_TOOLSGLOBAL_INC_BERECHNEVERSANDPREIS](hook_toolsglobal_inc_berechneversandpreis.md) -
vor der Berechnung weitergehender Kosten durch abhängige Versandarten, Zuschläge oder einer möglichen Deckelung.

## Parameter

* `float` **price** - Rückgabe des berechneten Versandpreises
* `JTL\Checkout\Versandart|object` **shippingMethod** - Versandart
* `string` **iso** - ISO-Code der Währung
* `JTL\Catalog\Product\Artikel|stdClass` **additionalProduct** - Zusatzartikel
* `JTL\Catalog\Product\Artikel|null` **product** - Artikel für artikelabhängige Versandkosten