# HOOK_TOOLS_GLOBAL_PRUEFEARTIKELABHAENGIGEVERSANDKOSTEN (163)

## Triggerpunkt

Am Anfang der Prüfung der artikelabhängigen Versandkosten

## Parameter

* `JTL\Catalog\Product\Artikel` **&oArtikel** - Artikelobjekt
* `bool` **&bHookReturn** - Hookresultat (default `= false`)