# HOOK_BESTELLVORGANG_PAGE_STEPLIEFERADRESSE_NEUELIEFERADRESSE_PLAUSI (12)

## Triggerpunkt

Plausibilitätsprüfung nach Eingabe der neuen Lieferadresse

## Parameter

* `int` **&nReturnValue** - Ergebnis der Plausibilitätsprüfung
* `array` **&fehlendeAngaben** - Liste der Felder, die im Formular nicht ausgefüllt wurden