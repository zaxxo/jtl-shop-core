# HOOK_GET_NEWS (220)

## Triggerpunkt

Nach dem Holen aller News

## Parameter

* `bool` **cached** - Flag "wurde vom Zwischenspeicher gelesen?"
* `array` **cacheTags** - Umfang des Zwischenspeicherns
* `\Illuminate\Support\Collection` **oNews_arr** - Zusammenstellung der News