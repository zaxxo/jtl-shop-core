# Die Shop-Klasse

Der Klasse `Shop` kommt eine zentrale Bedeutung zu. Sie dient in erster Linie als zentrale Registry für ehemals
ausschließlich globale Variablen wie die *NiceDB* oder *Smarty*, dient aber auch der Erzeugung und Ausgabe von
Instanzen für den neuen Objektcache.

In JTL-Shop 5.x können und sollten nun Klasseninstanzen von *NiceDB* und *Smarty* über die `Shop`-Klasse bezogen
werden.

Ab JTL-Shop 5.0 wird folgende Vorgehensweise bevorzugt:

    $product = Shop::Container()->getDB()->queryPrepared(
       'SELECT * FROM tartikel WHERE kArtikel = :artID',
       ['artID' => $articleID],
       ReturnType::SINGLE_OBJECT
    );

Für Smarty wird in JTL-Shop 5.0 diese Vorgehensweise bevorzugt:

    Shop::Smarty()
        ->assign('myvar', 123)
        ->assign('myothervar', 'foobar')
        ->display('mytemplate.tpl');

Die Methode `JTLSmarty::assign(string $tpl_var, mixed $value)` wurde "*chainable*" gemacht, um die Übersichtlichkeit
im Code zu erhöhen.

## Dependency Injection

Um neuen Code testbar zu machen, können Abhängigkeiten dem Konstruktor übergeben werden. Sinnvoll ist es, im
Konstruktor sicherzustellen, dass die Dependency auch verfügbar ist, wenn sie nicht übergeben wurde.

## Sprachfunktionen

Auch *Sprachfunktionen* sollten nun über die *Shop*-Klasse genutzt werden.

    Shop::Lang()->get('basketAllAdded', 'messages');

## Caching

Die Nutzung des *Caches* erfolgt analog den Sprachfunktionen und wird im Kapitel [Cache](../shop_plugins/cache.md)
näher erläutert.

## Onlineshop-URL

Um die URL des Onlineshops zu beziehen, wurde die Methode `Shop::getURL([bool $bForceSSL = false]) : string`
eingeführt.

!!! attention
    Wir empfehlen dringend, diese Variante anstelle der veralteten Konstante `URL_SHOP` zu nutzen, da `Shop::getURL()`
    auch eine eventuelle Konfiguration von *SSL* berücksichtigt.

Die Ausgabe erfolgt stets **ohne abschließenden Slash**.

## Debugging

Die Funktion `Shop::dbg(mixed $content[, bool $die, string $prepend]) : void` erlaubt "quick-and-dirty" *Debugging*.

Als ersten Parameter erhält sie beliebigen Inhalt zur Ausgabe. Wird der zweite Parameter auf `true` gesetzt, kann die
weitere Ausführung des Codes unterbunden werden. Der dritte Parameter kann einen Text beinhalten, der vor der
Debug-Ausgabe als Erläuterung erscheinen soll. Dies entspricht im Wesentlichen einem von ``<pre>``-Tags umhüllten
`var_dump()` mit ggf. anschließendem `die()`.