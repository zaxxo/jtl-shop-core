# Breaking Changes

## JTL-Shop 5.4

- **Smarty auf Version 4.5 aktualisiert**

    Die Template-Engine *Smarty* wurde auf Version 4.5 aktualisiert, was folgende Änderungen bedeutet:

    * PHP-Funktionen die innerhalb einer Templatedatei verwendet werden sollen, müssen zuvor explizit via `Smarty::registerPlugin()` registriert werden.
    * Klassen müssen via `Smarty::registerClass()` ebenfalls registriert werden, wenn sie innerhalb von Templates verwendet werden sollen.

    Der Shop registriert bereits einen Großteil an PHP-Funktionen, sodass i.A. hier kein Handlungsbedarf bestehen sollte, es kann aber passieren, dass einzelne Funktionen fehlen.
    Im Falle des Zugriffs auf eine unregistrierte Klasse wird das Template eine Deprecation in der folgenden Art ausgeben:

    ```PHP Deprecated:  Using unregistered static method "\JTL\Alert\Alert::TYPE_PRIMARY" in a template is deprecated and will be removed in a future release. Use Smarty::registerClass to explicitly register a class for access.```

    Der Zugriff auf eine unregistrierte PHP-Funktion erzeugt eine ähnliche Meldung:
  
    ```PHP Deprecated:  Using unregistered function "get_html_translation_table" in a template is deprecated and will be removed in a future release. Use Smarty::registerPlugin to explicitly register a custom modifier.```

    Das genaue Vorgehen wird im [Demo Plugin](https://gitlab.com/jtl-software/jtl-shop/plugins/jtl_test) genauer gezeigt.

    *Achtung:* Sobald die Templatedatei einmal kompiliert wurde, werden diese Meldungen nicht nochmals angezeigt werden.
    Es ist daher ratsam, während der Entwicklung entweder manuell den Templatecache zu löschen
    oder die Konstante `SMARTY_FORCE_COMPILE` in der includes/config.JTL-Shop.ini.php auf `true` zu setzen.

## JTL-Shop 5.3

- **Der Standardzeichensatz wurde auf 4-Byte-UTF8 geändert**

    Für den Betrieb von JTL-Shop 5.3.x werden nur noch MySQL-/MariaDB-Versionen unterstützt, die variable Multibyte-Zeichensätze unterstützen.

    * MariaDB >= 10.6
    * MySQL >= 8.0

    Per Datenbank-Migration werden alle Tabellen auf *utf8mb4* mit der Kollation *utf8mb4_unicode_ci* umgestellt.
    Shop-Tabellen mit einem 3-Byte-Zeichensatz werden im Backend als fehlerhaft markiert.
    Für die [Migration von Plugin-Tabellen](../shop_plugins/hinweise.md#label_hinweise_dbmigration) kann die Methode *JTL\Update\DBMigrationHelper::migrateToInnoDButf8()* verwendet werden.

## JTL-Shop 5.x ⇒ 5.2.x

- **Systemvoraussetzung auf PHP 8.1 angehoben**

    Für den Betrieb von JTL-Shop 5.2.x ist PHP 8.1 Voraussetzung.
    Ab Version 5.2.3 ist JTL-Shop zusätzlich für PHP 8.2 freigegeben.

- **jQuery Version auf 3.1 angehoben**

    Mit JTL-Shop 5.2.x erfolgte für das Javascript-Framework *jQuery* ein Update von Version 3.0 auf
    Version 3.1.

- **CLI unterstützt jetzt auch die Generierung der Bilder**

    Neues CLI-Kommando: cache:images:create

- **Neuer Hook HOOK_ORDER_DOWNLOAD_FILE (402)**

- **Kategoriebaum wird bei deaktiviertem Cache nicht mehr in der Session gespeichert**

- **Redis 6 mit ACL Benutzername+Passwort wird jetzt unterstützt**

- **Systemcheck Erweiterung: Info, welche Caching-Methoden serverseitig verfügbar sind**

- **Die Versionierung wurde geändert**

    * "Semantic Versioning" : für JTL-Shop,
    * "API-Versioning" : intern für den Abgleich mit JTL-Wawi**

    Mit JTL-Shop 5.x wird die Versionsnummerierung des Onlineshops auf das allgemein gültige Verfahren
    [SemVer](http://semver.org/) umgestellt. Für die Verbindung zur JTL-Wawi wird intern weiterhin die bisherige
    Versionierung als interne API-Version geführt.

- **Die Upgrade-Möglichkeit von JTL-Shop *kleiner* Version 4.02 auf Version 5.x wurde entfernt**

    Nutzer vorheriger Versionen (zum Beispiel Version und kleiner 3.0x) müssen auf JTL-Shop Version 4.06
    aktualisieren, um von dort auf JTL-Shop Version 5.x upgraden zu können.

- **Das von JTL-Shop 4 bekannte Template "Evo" wird ab JTL-Shop 5.x als separates Projekt geführt
  und ist nicht mehr im Lieferumfang von JTL-Shop enhalten**

    Sie finden das Template "Evo" im JTL-Repository auf gitlab unter
    [Evo](https://gitlab.com/jtl-software/jtl-shop/templates) und auf dem JTL Builds-Server unter
    [build.jtl-shop.de](https://build.jtl-shop.de/get/template_evo-5-0-0-rc-3.zip/template)

- **Werkzeuge zum Kompilieren von Themes überarbeitet**

    In JTL-Shop 5.x werden Themes mit dem
    [JTL Theme Editor](https://gitlab.com/jtl-software/jtl-shop/plugins/jtl_theme_editor) übersetzt.

    Weitere Informationen zur Verwendung dieser Plugins finden Sie im Abschnitt
    [Eigenes Theme](../shop_templates/eigenes_theme.md).

- **Hooks erweitert/ergänzt/entfernt**

    Im Zuge der hier genannten Anpassungen und Änderungen haben sich auch verschiedene Hooks des Plugin-Systems
    geändert, wurden ergänzt oder sind ganz weggefallen. Eine komplette Liste aller aktuell verfügbaren Hooks und ihrer
    Parameter finden Sie hier in der
    Entwicklerdokumentation unter [Hook-Liste](../shop_plugins/hook_list.md).
